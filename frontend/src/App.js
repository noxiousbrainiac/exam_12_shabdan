import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import {useSelector} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Photos from "./containers/Photos/Photos";
import NewPhoto from "./containers/NewPhoto/NewPhoto";
import UserGallery from "./containers/UserGallery/UserGallery";

const App = () => {
  const user = useSelector(state => state.users.user);

  const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props}/> :
        <Redirect to={redirectTo}/>
  };

  return (
      <Layout>
          <Switch>
              <Route exact path='/' component={Photos}/>
              <Route path='/user/:id' component={UserGallery}/>
              <Route path="/register" component={Register}/>
              <Route path="/login" component={Login}/>
              <ProtectedRoute
                path="/newPhoto"
                component={NewPhoto}
                isAllowed={user}
                redirectTo={Login}
              />
          </Switch>
      </Layout>
  );
};

export default App;