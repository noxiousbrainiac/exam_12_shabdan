import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import {MuiThemeProvider} from "@material-ui/core";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import store from "./store/storeConfigurations";
import theme from "./theme";
import history from "./history";

const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <ToastContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
