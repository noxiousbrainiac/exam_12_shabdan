import React, {useState} from 'react';
import {Grid, TextField} from "@material-ui/core";
import {addPhoto} from "../../store/actions/photosActions";
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const NewPhoto = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.photos.postError);
    const loading = useSelector(state => state.photos.postLoading);
    const [state, setState] = useState({
        title: "",
        image: null
    });

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    }

    const inputChangeHandler = (e) => {
        const {name, value} = e.target;
        setState(prev => ({
            ...prev,
            [name]: value,
        }));
    }

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(state).map(item => {
            formData.append(item, state[item]);
        })
        dispatch(addPhoto(formData));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid
            component="form"
            container
            direction="column"
            spacing={2}
            onSubmit={submitFormHandler}
            style={{paddingTop: "20px"}}
        >
            <h1>New Photo</h1>
            <Grid item lg={6} md={6} xs={12}>
                <FormElement

                    label="Title"
                    name="title"
                    value={state.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />
            </Grid>

            <Grid item lg={6} md={6} xs={12}>
                <TextField
                    type="file"
                    name="image"
                    onChange={fileChangeHandler}
                />
            </Grid>

            <Grid item lg={4} md={4} xs={4}>
                <ButtonWithProgress
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    loading={loading}
                    disabled={loading}
                >
                    Add
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default NewPhoto;