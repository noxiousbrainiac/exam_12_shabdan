import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {clearErrors, getPhotosByUser} from "../../store/actions/photosActions";
import {Container, LinearProgress} from "@material-ui/core";
import PhotosList from "../../components/Lists/PhotosList/PhotosList";

const UserGallery = ({match}) => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.photos.getLoading);

    useEffect(() => {
        dispatch(getPhotosByUser(match.params.id));

        return () => {
            dispatch(clearErrors());
        }
    }, [dispatch, match.params.id]);

    let content = (<PhotosList photos={photos}/>);

    if (loading === true) {
        content = <LinearProgress style={{width: "100%", marginTop: "40%"}}/>
    }

    return (
        <Container>
            {content}
        </Container>
    );
};

export default UserGallery;