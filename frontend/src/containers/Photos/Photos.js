import React, {useEffect} from 'react';
import {Container, LinearProgress} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import PhotosList from "../../components/Lists/PhotosList/PhotosList";
import {clearErrors, getPhotos} from "../../store/actions/photosActions";

const Photos = () => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.photos.getLoading);

    useEffect(() => {
        dispatch(getPhotos());
        return () => {
            dispatch(clearErrors());
        }
    }, [dispatch]);

    let content = (<PhotosList photos={photos}/>);

    if (loading === true) {
        content = <LinearProgress style={{width: "100%", marginTop: "40%"}}/>
    }

    return (
        <Container style={{position: "relative"}}>
            {content}
        </Container>
    );
};

export default Photos;