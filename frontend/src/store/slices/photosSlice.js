import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    photos: [],
    postError: null,
    postLoading: false,
    getLoading: false,
    getError: null,
};

const name = 'photos';

const photosSlice = createSlice({
    name,
    initialState,
    reducers: {
        addPhoto(state, action) {
            state.postLoading = true;
        },
        addPhotoSuccess(state, action) {
            state.postLoading = false;
        },
        addPhotoFailure(state, action) {
            state.postLoading = false;
            state.postError = action.payload;
        },
        getPhotos(state, action) {
            state.getLoading = true;
        },
        getPhotosSuccess(state, action) {
            state.getLoading = false;
            state.photos = action.payload;
        },
        getPhotosFailure(state, action) {
            state.getError = action.payload;
            state.getLoading = false;
        },
        getPhotosByUser(state, action) {
            state.getLoading = true;
        },
        getPhotosByUserSuccess(state, action) {
            state.getLoading = false;
            state.photos = action.payload;
        },
        getPhotosByUserFailure(state, action) {
            state.getError = action.payload;
            state.getLoading = false;
        },
        deletePhoto(state, action) {
            state.postLoading = true;
        },
        deletePhotoSuccess(state, action) {
            state.postLoading = false;
        },
        deletePhotoFailure(state, action) {
            state.postLoading = false;
            state.postError = action.payload;
        },
        clearErrors(state, action) {
            state.postError = null;
            state.getError = null;
        },
    }
});

export default photosSlice;