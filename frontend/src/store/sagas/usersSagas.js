import {put, takeEvery} from "redux-saga/effects";
import {
    facebookLogin,
    loginUser,
    loginUserFailure,
    loginUserSuccess, logout,
    registerUser,
    registerUserFailure,
    registerUserSuccess,
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* registerUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        yield put(historyPush('/'));
        toast.success('Registered successful!');
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(registerUserFailure(e.response.data));
    }
}

export function* loginUserSaga({payload: user}) {
    try {
        const response = yield axiosApi.post('/users/sessions', user);
        yield put(loginUserSuccess(response.data));
        yield put(historyPush('/'));
        toast.success('Login successful!');
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(loginUserFailure(e.response.data));
    }
}

export function* logoutUserSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
    } catch (e) {
        if (e.response && e.response.data) {
            yield  put(loginUserFailure(e.response.data));
        } else {
            yield put(loginUserFailure({message: "No internet connexion"}));
        }
    }
}

export function* facebookLoginSaga({payload: data}) {
    try {
        const response = yield axiosApi.post('/users/facebookLogin', data);
        yield put(loginUserSuccess(response.data));
        yield put(historyPush('/'));
        toast.success('Login successful!');
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(loginUserFailure(e.response.data));
    }
}

const usersSaga = [
    takeEvery(registerUser, registerUserSaga),
    takeEvery(loginUser, loginUserSaga),
    takeEvery(logout, logoutUserSaga),
    takeEvery(facebookLogin,facebookLoginSaga)
];

export default usersSaga;