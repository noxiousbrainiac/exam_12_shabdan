import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";
import {
    addPhoto, addPhotoFailure,
    addPhotoSuccess, deletePhoto, deletePhotoFailure, deletePhotoSuccess,
    getPhotos,
    getPhotosByUser, getPhotosByUserFailure,
    getPhotosByUserSuccess, getPhotosFailure,
    getPhotosSuccess
} from "../actions/photosActions";

export function* addPhotoSaga({payload: photoData}) {
    try {
        yield axiosApi.post(`/photos`, photoData);
        yield put(addPhotoSuccess());
        yield put(historyPush('/'));
        toast.success("Photo added!");
    } catch (e) {
        yield put(addPhotoFailure(e.response.data));
        toast.error(e.response.data.global);
    }
}

export function* getPhotosSaga() {
    try {
        const response = yield axiosApi.get(`/photos`);
        yield put(getPhotosSuccess(response.data));
    } catch (e) {
        yield put(getPhotosFailure(e.response.data));
        toast.error(e.response.data.global);
    }
}

export function* getPhotosByUserSaga({payload: userId}) {
    try {
        const response = yield axiosApi.get(`/photos/user/${userId}`);
        yield put(getPhotosByUserSuccess(response.data));
    } catch (e) {
        yield put(getPhotosByUserFailure(e.response.data));
        toast.error(e.response.data.global);
    }
}

export function* deletePhotoSaga({payload: photoId}) {
    try {
        yield axiosApi.delete(`/photos/${photoId}`);
        yield put(deletePhotoSuccess());
        yield put(getPhotos());
        yield put(historyPush('/'));
    } catch (e) {
        yield put(deletePhotoFailure(e));
        toast.error(e.response.data.error);
    }
}

const usersSaga = [
    takeEvery(addPhoto, addPhotoSaga),
    takeEvery(getPhotos, getPhotosSaga),
    takeEvery(getPhotosByUser, getPhotosByUserSaga),
    takeEvery(deletePhoto, deletePhotoSaga),
];

export default usersSaga;