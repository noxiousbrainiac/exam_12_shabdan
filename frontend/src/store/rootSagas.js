import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import photosSagas from "./sagas/photosSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...photosSagas,
    ])
}