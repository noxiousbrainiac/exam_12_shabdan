import photosSlice from "../slices/photosSlice";

export const {
    addPhoto,
    addPhotoSuccess,
    addPhotoFailure,
    getPhotos,
    getPhotosSuccess,
    getPhotosFailure,
    getPhotosByUser,
    getPhotosByUserSuccess,
    getPhotosByUserFailure,
    deletePhoto,
    deletePhotoSuccess,
    deletePhotoFailure,
    clearErrors
} = photosSlice.actions;