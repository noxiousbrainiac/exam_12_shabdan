import React from 'react';
import {Grid} from "@material-ui/core";
import PhotoItem from "./PhotoItem/PhotoItem";

const PhotosList = ({photos}) => {
    return (
        <Grid container spacing={2} style={{paddingTop: "20px"}}>
            {photos.length !== 0 ?
                photos.map(i => (
                    <PhotoItem
                        key={i._id}
                        id={i._id}
                        title={i.title}
                        image={i.image}
                        displayName={i.user.displayName}
                        userId={i.user._id}
                    />
                )) : <h1>No photos</h1>
            }
        </Grid>
    );
};

export default PhotosList;