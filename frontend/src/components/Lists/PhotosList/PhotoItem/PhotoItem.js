import React, {useState} from 'react';
import {Card, CardActions, CardHeader, CardMedia, Grid, IconButton, Link, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import ButtonWithProgress from "../../../UI/ButtonWithProgress/ButtonWithProgress";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {deletePhoto} from "../../../../store/actions/photosActions";
import {apiURL} from "../../../../config";
import ModalWindow from "../../../UI/ModalWindow/ModalWindow";

const useStyles = makeStyles({
    card: {
        height: '100%',
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});

const PhotoItem = ({id, image, title, displayName, userId}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();
    const loading = useSelector(state => state.photos.postLoading);

    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const remove = () => {
        dispatch(deletePhoto(id));
    }

    let cardImage = apiURL + '/' + image;

    return (
        <>
            <ModalWindow image={image} title={title} open={open} onClose={() => handleClose()}/>
            <Grid item xs={12} sm={6} md={6} lg={4}>
                <Card>
                    <CardHeader
                        title={title}
                    />
                    <CardActions>
                        <IconButton component={"button"} onClick={handleOpen}>
                            <ArrowForwardIcon />
                        </IconButton>
                        {user?._id === userId  ?
                            <ButtonWithProgress
                                onClick={remove}
                                variant="contained"
                                color="secondary"
                                loading={loading}
                                disabled={loading}
                            >
                                Remove
                            </ButtonWithProgress>
                            : null
                        }
                        <Link href={`/user/${userId}`} underline="hover">
                            By {displayName}
                        </Link>
                    </CardActions>
                    <CardMedia
                        image={cardImage}
                        className={classes.media}
                    />
                </Card>
            </Grid>
        </>
    );
};

export default PhotoItem;