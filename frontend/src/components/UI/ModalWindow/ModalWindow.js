import React from 'react';
import {Button, Card, CardHeader, CardMedia, makeStyles, Modal} from "@material-ui/core";
import {apiURL} from "../../../config";

const useStyles = makeStyles({
    card: {
        position: "absolute",
        height: '530px',
        top: "10%",
        left: "50%",
        transform: "translateX(-50%)",
        width: "600px"
    },
    media: {
        height: 0,
        paddingTop: '64.25%'
    }
})

const ModalWindow = ({image, title, open, onClose}) => {
    const classes = useStyles();

    let cardImage = apiURL + '/' + image;
    return (
        <Modal
            open={open}
            onClose={onClose}
        >
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia image={cardImage} className={classes.media}/>
                <div style={{textAlign: "center", margin: "20px 0"}}>
                    <Button onClick={onClose} variant="contained" color="secondary">
                        Close
                    </Button>
                </div>
            </Card>
        </Modal>
    );
};

export default ModalWindow;