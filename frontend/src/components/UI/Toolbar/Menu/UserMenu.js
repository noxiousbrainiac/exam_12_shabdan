import React, {useState} from 'react';
import {Button, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {Link} from 'react-router-dom';
import {logout} from "../../../../store/actions/usersActions";

const useStyles = makeStyles({
    menuColor: {
        color: "beige"
    }
})

const UserMenu = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const toLogOut = () => {
        dispatch(logout());
    }

    return (
        <>
            <Button
                className={classes.menuColor}
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
            >
                Hello, {user.displayName}!
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose} component={Link} to={'/newPhoto'}>New Photo</MenuItem>
                <MenuItem onClick={toLogOut}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;