const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require('./models/User');
const Photo = require('./models/Photo');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [admin, user] = await User.create({
        email: 'admin@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin',
        avatar: "https://sm.ign.com/t/ign_nl/news/d/daisy-ridl/daisy-ridley-on-why-baby-yoda-will-save-us-all_vmj9.h720.jpg"
    }, {
        email: 'user@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'user',
        displayName: 'User',
        avatar: "https://hips.hearstapps.com/digitalspyuk.cdnds.net/17/09/1488367706-star-wars-7-the-force-awakens-kylo-ren.jpg",
    });

    await Photo.create({
        title: "Apple office",
        user: admin,
        image: "fixtures/apple.jpg",
    }, {
        title: "Car",
        user: admin,
        image: "fixtures/car.jpg",
    }, {
        title: "Lake",
        user: user,
        image: "fixtures/lake.jpg",
    }, {
        title: "Sky",
        user: user,
        image: "fixtures/sky.jpg",
    }, {
        title: "Town",
        user: user,
        image: "fixtures/town.jpg",
    },)


    await mongoose.connection.close();
};

run().catch(console.error);