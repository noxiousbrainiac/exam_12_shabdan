require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./app/users');
const photos = require('./app/photos');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8080;

app.use('/users', users);
app.use('/photos', photos);

const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(port, () => {
        console.log('Port start on port: ', port);
    });

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
}

run().catch(e => console.log(e));