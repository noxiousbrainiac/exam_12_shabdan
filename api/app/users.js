const express = require('express');
const User = require('../models/User');
const {nanoid} = require("nanoid");
const axios = require('axios');
const config = require('../config');

const router = express.Router();

router.post('/' , async (req, res) => {
    try {
        const userData = {
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.displayName,
        }

        const user = new User(userData);
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/sessions', async (req, res) => {
    try {
        try {
            const user = await User.findOne({email: req.body.email});

            if (!user) return res.status(401).send({message: "Data is not correct"});

            const isMatch = await user.checkPassword(req.body.password);

            if (!isMatch) return res.status(401).send({message: "Data is not correct"});

            user.generateToken();
            await user.save({validateBeforeSave: false});
            res.send(user);
        } catch (e) {
            res.status(500).send(e);
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/sessions', async (req, res) => {
    try {
        const token = req.get('Authorization');
        const success = {message: 'Success'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();

        await user.save({validateBeforeSave: false});

        return res.send(success);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;

    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({global: 'Facebook token incorrect'});
        }

        if (req.body.id !== response.data.data.user_id) {
            return res.status(401).send({global: 'Wrong User Id'});
        }

        let user = await User.findOne({email: req.body.email});

        if (!user) {
            user = await User.findOne({facebookId: req.body.id});
        }

        if (!user) {
            user = new User({
                email: req.body.email || nanoid(),
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
            });
        }

        user.generateToken();
        user.save({validateBeforeSave: false});

        res.send({message: 'Success', user});
    } catch (e) {
        res.status(401).send({global: 'Facebook token incorrect!'});
    }
});

module.exports = router;