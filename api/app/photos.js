const express = require('express');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const auth = require('../middleware/auth');
const Photo = require('../models/Photo');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [auth, upload.single('image')], async (req, res) => {
   try {
        const photoData = {
            title: req.body.title,
            user: req.user._id,
        };

       if (req.file) {
           photoData.image = 'uploads/' + req.file.filename;
       }

       const photo = new Photo(photoData);
       await photo.save();
       res.send(photo);
   } catch (e) {
       res.status(500).send(e);
   }
});

router.get('/', async (req, res) => {
    try {
        const photos = await Photo.find().populate("user", "displayName");

        res.send(photos)
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/user/:id', async (req, res) => {
    try {
        const photos = await Photo.find({user: req.params.id}).populate('user', 'displayName');

        res.send(photos)
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const photo = await Photo.findById(req.params.id);

        if (req.user._id.toString() !== photo.user.toString()) {
            return res.status(403).send({error: "You can't delete this photo!"});
        }

        if (photo) {
            await Photo.findByIdAndDelete(req.params.id);
            return res.send("Photo was deleted!");
        } else {
            return res.status(404).send({message: "Photo not found!"});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;